function createNewUser (firstName = prompt('Enter your first name:', 'Alice'),
                        lastName = prompt('Enter your last name:','Kovalski')) {
  const newUser = {};

  Object.defineProperty(newUser, 'firstName', {
    value: firstName,
    writable: false,
    configurable: true
  });

  Object.defineProperty(newUser, 'lastName', {
    value: lastName,
    writable: false,
    configurable: true
  });

  newUser.setFirstName = function (newFirstName) {
    Object.defineProperty(newUser, 'firstName', {
      value: newFirstName
    });
  };

  newUser.setLastName = function (newLastName) {
    Object.defineProperty(newUser, 'lastName', {
      value: newLastName
    });
  };

  newUser.getLogin = function() {
    return (newUser.firstName.slice(0, 1) + newUser.lastName).toLowerCase();
  };
  return newUser
}

const user = createNewUser();
console.log(user);
console.log(user.getLogin());