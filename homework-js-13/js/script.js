const changeThemeBtn = document.querySelector('.change-theme');

const changeTheme = event =>{
  document.querySelectorAll('.alt-theme').forEach(elem => elem.classList.toggle('dark-theme'));
  if (changeThemeBtn.classList.contains('dark-theme')) {
    changeThemeBtn.innerText = 'No thanks, go back';
    localStorage.setItem('userTheme', 'dark');
  } else {
    changeThemeBtn.innerText = 'Try our new dark theme!';
    localStorage.setItem('userTheme', 'standart');
  }
};

changeThemeBtn.addEventListener('click', changeTheme);

if (localStorage.getItem('userTheme') === 'dark') {
  changeThemeBtn.innerText = 'No thanks, go back';
  changeTheme()
}