const randomColor = () =>
    `rgb(${Math.round(Math.random()*255)}, 
         ${Math.round(Math.random()*255)}, 
         ${Math.round(Math.random()*255)})`;

const button = document.querySelector('button');
const input = document.createElement('input');
input.placeholder = 'Enter diameter';
input.value = '10';

const circles = document.createElement('section');
const circle = document.createElement('div');
circle.className = 'circle';

button.addEventListener('click', event => {
  if (!document.querySelector('input')) {
    button.before(input);
    document.querySelector('script').before(circles);
  } else {
    circle.style.width = input.value + 'px';
    circle.style.height = input.value + 'px';
    document.querySelectorAll('section > div').forEach(item => item.remove());
    for (let i = 0; i < 100; i++) {
      circle.style.backgroundColor = randomColor();
      circles.appendChild(circle.cloneNode());
    }
  }
});

circles.addEventListener('click', event => {
  if (event.target !== event.currentTarget) {
    event.target.remove();
  }
});
