let nameUser = prompt('What is your name?');
while (!isNaN(nameUser)) {
    nameUser = prompt('Error. Enter your name again:', nameUser);
}

let ageUser = +prompt('What is your age?');
while (isNaN(ageUser) || !ageUser) {
    ageUser = prompt('Error. Enter your age again:', ageUser);
}

if (ageUser < 18) {
    alert('You are not allowed to visit this website.');
} else if (ageUser >= 18 && ageUser <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert('Welcome, ' + nameUser);
    } else alert('You are not allowed to visit this website');
} else if (ageUser > 22) {
    alert('Welcome, ' + nameUser);
} else {
    alert('You are not allowed to visit this website');
}