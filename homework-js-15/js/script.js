const $btn = $('.to-top');
$('.paralax').scroll(function () {
  if ($('.paralax').scrollTop() > $('.paralax').innerHeight()) {
    $btn.fadeIn('slow')
  } else  {
    $btn.fadeOut('slow')
  }
});

$btn.click(function () {
  $('.paralax').animate({scrollTop: 0}, 'slow');
});

$('.navigation-item').click(function (event) {
  event.preventDefault();
  const $href = $(this).attr('href');
  const $top = $($href).offset().top;
  $('.paralax').animate({scrollTop: $top}, 'slow');
});

$('.slide-toggle').click(function () {
  $('.top-posts').slideToggle();
});