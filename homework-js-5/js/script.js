function createNewUser (firstName = prompt('Enter your first name:', 'Alice'),
                        lastName = prompt('Enter your last name:','Kovalski'),
                        customBirthday = prompt('Enter your birthday in \'dd.mm.yyyy\' format','21.01.2000')) {
  const newUser = {};

  Object.defineProperty(newUser, 'firstName', {
    value: firstName,
    writable: false,
    configurable: true
  });

  Object.defineProperty(newUser, 'lastName', {
    value: lastName,
    writable: false,
    configurable: true
  });

  const splitBirthday = customBirthday.split('.');
  newUser.birthday = new Date(Number(splitBirthday[2]), Number(splitBirthday[1])-1, Number(splitBirthday[0]));


  newUser.setFirstName = function (newFirstName) {
    Object.defineProperty(newUser, 'firstName', {
      value: newFirstName
    });
  };

  newUser.setLastName = function (newLastName) {
    Object.defineProperty(newUser, 'lastName', {
      value: newLastName
    });
  };

  newUser.getLogin = function() {
    return (newUser.firstName.slice(0, 1) + newUser.lastName).toLowerCase();
  };

  newUser.getPassword = function() {
    return newUser.firstName.slice(0, 1).toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthday.getFullYear();
  };

  newUser.getAge = function () {
    const today = new Date();
    let age = Number(today.getFullYear() - newUser.birthday.getFullYear());
    if (today.getMonth() <= newUser.birthday.getMonth()) {
      if (today.getDate() < newUser.birthday.getDate()) {
        age--;
      }
    }
    return age;
  };
  return newUser
}

const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

