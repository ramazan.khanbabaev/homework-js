document.querySelector('.tabs').addEventListener('click', event => {
  document.querySelector('.tabs-title.active').classList.remove('active');
  event.target.classList.add('active');

  document.querySelector('.tab-content.active').classList.remove('active');
  document.querySelector('.tab-content[data-tab-title="' + event.target.dataset.tabTitle + '"]').classList.add('active');
});