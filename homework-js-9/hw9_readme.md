## Задание

Реализовать переключение вкладок (табы) на чистом Javascript.

#### Технические требования:
- В папке `tabs` лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки. 
- Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
- Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

#### Литература:
- [HTMLElement.dataset](https://developer.mozilla.org/ru/docs/Web/API/HTMLElement/dataset)

// const tabs = document.querySelector('.tabs');
// const tabsContent = document.querySelector('.tabs-content');
//
// tabs.addEventListener('click', event => {
//   document.querySelector('.tabs-title.active').classList.remove('active');
//   event.target.classList.add('active');
//   for (let i = 0; i < event.currentTarget.children.length; i++) {
//     if (!event.currentTarget.children[i].classList.contains('active')) {
//       tabsContent.children[i].classList.remove('tab-content-active');
//     } else {
//       tabsContent.children[i].classList.add('tab-content-active');
//     }
//   }
// });