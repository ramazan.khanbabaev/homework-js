const student = {
  firstName: 'Ramazan',
  lastName: 'Khanbabaev',
  tabel: {
    math: '11',
    language: '10',
    physicalCulture: '12',
    english: {
      reading: '10',
      writing: '9',
      listening: '8',
      grammar: '11'
    }
  },
  additions: ['Approximate behavior', 'Active', {
    mathOlympiad: '1st place',
    languageOlympiad: '3rd place',
    physicsOlympiad: '4th place'
  }]
};

function cloneObject(object) {
  if (Array.isArray(object)) {
    const array = [];
    for (let item of object) {
      array.push(cloneObject(item));
    }
    return array
  } else if (typeof object === "object") {
    const newObject = {};
    for (let field in object) {
      newObject[field] = cloneObject(object[field])
    }
    return newObject
  } else {
    return object;
  }
}

console.log(student);
cloneStudent = cloneObject(student);
console.log(cloneStudent);