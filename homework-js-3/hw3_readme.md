## Теоретический вопрос 

1. Описать своими словами для чего вообще нужны функции в программировании. 
2. Описать своими словами, зачем в функцию передавать аргумент.

## Задание

Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. 

#### Технические требования:
V Считать с помощью модального окна браузера два числа. 
V Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено `+`, `-`, `*`, `/`.
V Создать функцию, в которую передать два значения и операцию.
V Вывести в консоль результат выполнения функции.

#### Не обязательное задание продвинутой сложности:
V После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

#### Литература:
- [Функции - основы](https://learn.javascript.ru/function-basics)