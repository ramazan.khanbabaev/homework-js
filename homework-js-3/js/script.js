let number1 = prompt('Enter first number:', '4');
let number2 = prompt('Enter second number:', '2');

while (isNaN(number1) || !number1 || isNaN(number2) || !number2) {
    number1 = prompt('Error. Enter first number again:', number1);
    number2 = prompt('Error. Enter second number again:', number2);
}

number1 = +number1;
number2 = +number2;

let operation = prompt('Input the operation', '+');
while (operation !== '+' && operation !== '-' && operation !== '/' && operation !== '*') {
    operation = prompt('Error. Input the operation again', '+');
}

function calculate(number1, number2, operation) {
    switch (operation) {
        case '+':
            return number1 + number2;
        case '-':
            return number1 - number2;
        case '/':
            return number1 / number2;
        case '*':
            return number1 * number2;
    }
}

console.log(`${number1} ${operation} ${number2} = ${calculate(number1, number2, operation)}`);