document.querySelectorAll('.fas.icon-password').forEach(icon => {
  icon.addEventListener('click', event => {
    event.currentTarget.classList.toggle('fa-eye');
    event.currentTarget.classList.toggle('fa-eye-slash');

    event.currentTarget.classList.contains('fa-eye')
        ? event.currentTarget.previousElementSibling.type = 'text'
        : event.currentTarget.previousElementSibling.type = 'password';
  });
});

const password = document.querySelector('input');
const repeatPassword = document.querySelectorAll('input')[1];
const warning = document.createElement('span');
warning.innerText = 'Need same values';
warning.className = 'warning';

document.querySelector('.btn').addEventListener('click', event => {
  event.preventDefault();
  if (password.value === repeatPassword.value && password.value) {
    warning.remove();
    alert('You are welcome')
  } else if (!password.value) {
    alert('Fill the fields')
  } else {
    repeatPassword.after(warning);
  }
});