const filterBy = (array, type) => {
  const filteredArray = [];
  array.forEach((item) => {
    if (typeof item !== type) {
      filteredArray.push(item);
    }
  });
  return filteredArray;
};

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));