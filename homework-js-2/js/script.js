let number = prompt('Enter integer number:', '12');
while (isNaN(number) || !number || parseInt(number) !== +number) {
    // For cancel entering
    if (number === null) {
        break;
    }
    number = prompt('Error. Enter integer number again:', number);
}

number = +number;

if (number < 5) {
    console.log('Sorry, no numbers');
} else {
    for (let i = 5; i <= number; i += 5) {
        console.log(i);
    }
}