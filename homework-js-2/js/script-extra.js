let m = prompt('Begin of array:', '2');
let n = prompt('End of array:', '11');

while (isNaN(m) || !m || parseInt(m) !== +m ||
        isNaN(n) || !n || parseInt(n) !== +n) {
    m = prompt('Error. Begin of array:', m);
    n = prompt('Error. End of array:', n);
}

m = +m;
n = +n;

// Because there aren't prime numbers before 2
if (m <= 2) {
    console.log(1);
    console.log(2);
    m = 3;
}

if (m > n) {
    console.log('Sorry, no numbers');
} else {
    for (let i = m, isPrime = true; i <= n; i++) {
        for (let j = 2; j < i; j++) {
            if (i % j === 0) {
                isPrime = false;
            }
        }
        if (isPrime === true) {
            console.log(i);
        }
        isPrime = true;
    }
}

