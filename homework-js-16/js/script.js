function factorial(number) {
    if (number === 1) {
        return 1;
    } else {
        return number * factorial(number - 1);
    }
}

let number = prompt('Enter the number:', '3');
while (isNaN(number) || !number || +number < 1 || parseInt(number) !== +number) {
    if (+number < 1) {
        alert('It is impossible to calculate factorial for this number');
    }
    number = prompt('Error. Enter the number again:', number);
}

number = +number;

alert(`Factorial of number '${number}' is ${factorial(number)}`);