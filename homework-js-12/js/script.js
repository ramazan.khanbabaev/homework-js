const images = document.querySelectorAll('.image-to-show');
let switchTime = 5;
const timer = document.querySelector('span');
timer.innerText = switchTime;

let imgId = 0;
const switchImg = () => {
  imgId + 1 === images.length ? imgId = 0 : imgId++;    //for cycle
  fadeOut(document.querySelector('.active'));
  setTimeout(() => fadeIn(images[imgId]), 250);
};

const fadeOut = img => {
  img.style.opacity = '1';
  const opacityInterval = setInterval(() => {
    img.style.opacity = img.style.opacity - 0.1;
    if (img.style.opacity === '0') {
      clearInterval(opacityInterval)
    }
  }, 25);
  setTimeout(() => img.classList.remove('active'), 250);
};

const fadeIn = img => {
  img.classList.add('active');
  img.style.opacity = '0';
  const opacityInterval = setInterval(() => {
    img.style.opacity = +img.style.opacity + 0.1;
    if (img.style.opacity === '1') {
      clearInterval(opacityInterval)
    }
  }, 25);
};

const timerClock = () => {
  timer.innerText = (timer.innerText - 0.01).toFixed(2);
  if (timer.innerText === '0.00') {
    timer.innerText = switchTime;
  }
};

fadeIn(images[0]);
let switchInterval = setInterval(switchImg, switchTime * 1000);
let timerInterval = setInterval(timerClock, 10);
let timeout;    //to continue from time when stop

document.querySelector('#stopBtn').addEventListener('click', () => {
  clearInterval(switchInterval);
  clearInterval(timerInterval);
  clearInterval(timeout);
});

document.querySelector('#continueBtn').addEventListener('click', () => {
  timerInterval = setInterval(timerClock, 10);
  timeout = setTimeout(() => {
    switchImg();
    switchInterval = setInterval(switchImg, switchTime * 1000);
    clearInterval(timeout);
  }, timer.innerText * 1000);
});

