function fibonacci(f0, f1, n) {
    if (n === 0) {
        return f0;
    } else if (n === 1) {
        return f1;
    } else if (n > 1) {
        return fibonacci(f0, f1, n - 1) + fibonacci(f0, f1, n - 2);
    } else if (n < 1) {
        return fibonacci(f0, f1, n + 2) - fibonacci(f0, f1, n + 1);
    }
}

let n = prompt('Enter index number of Fibonacci sequence:', '5');
while (isNaN(n) || !n || parseInt(n) !== +n) {
    n = prompt('Error. Enter index number of Fibonacci sequence again:', n);
}

n = +n;

alert(`Number with index '${n}' is ${fibonacci(0, 1, n)}`);

//-8 5 -3 2 -1 1 0 1 1 2 3 5 8