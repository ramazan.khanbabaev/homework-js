const estimate = (team, tasks, deadline) => {
  const workTime = Math.ceil(tasks.reduce((sum, taskWorkload) => sum + taskWorkload) / team.reduce((sum, workerSpeed) => sum + workerSpeed));
  //count how many hours left
  const now = new Date();
  now.setHours(now.getHours() + 1, 0, 0, 0); // to exclude current hour
  let hoursLeft = 0;
  while (+now !== +deadline) {
    if (now.getDay() > 0 && now.getDay() <= 5 && now.getHours() >= 10 && now.getHours() < 18) {
      hoursLeft++;
    }
    now.setHours(now.getHours() + 1);
  }
  console.log(`Work hours left to deadline: ${hoursLeft}`);
  console.log(`Team needs ${workTime} hours for work`);

  return hoursLeft > workTime ?
      `All tasks will successfully execute for ${Math.ceil((hoursLeft - workTime) / 24)} days before deadline!`
      : `Team needs ${workTime - hoursLeft} addition hours after deadline to execute all tasks in backlog`
};

console.log(estimate([1, 2, 3, 4], [60, 70, 50, 82], new Date(2019, 5, 26, 16)));