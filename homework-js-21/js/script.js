const toyota = {
  name: 'Toyota',
  description: 'A car',
  type: {
    name: 'Hatchback',
    transmission: [
      {
        type: 'automatic'
      },
      {
        type: 'manual'
      }
    ]
  },
  locales: [{
    name: 'en_US'
  }, {
    name: 'ru_RU'
  }, {
    anotherName: 'uk_UA'
  }
  ]
};

const bmw = {
  name: 'BMW',
  description: 'Germany car',
  type: {
    name: 'Sedan'
  },
  locales: [{
    name: 'en_US'
  },
  [
      [
        {
          deep: {
            name:'es_ES'
          }
        }
      ]
  ]
  ]
};

const lanos = {
  name: 'Ukrainian car',
  description: 'Lanos',
  type: {
    name: 'Sedan'
  },
  locales: {
    name: 'uk_UA'
  }
};

//Функция для поиска одного ключа в обьекте. С помощью рекурсии проиходим к этапу когда можно
// произвести поиск в самом "глубоком" обьекте. При первом совпадении, конец работы функции.
function searchKeys(object, keyWord, searchFields, path = '') {
  if (Array.isArray(object)) {
    for (let obj of object) {
      if (searchKeys(obj, keyWord, searchFields, path) === true) {
        return true
      }
    }
  } else {
    for (let field in object) {
      if (typeof object[field] === "object") {  //если в поле записан обьект
        path += field + '.';                    //то начинаем записывать его расположение
        if (searchKeys(object[field], keyWord, searchFields, path) === true) {
          return true
        } else path = '';
      } else if (object[field].toLowerCase().includes(keyWord.toLowerCase())) {
        path += field + '.';
        for (let field of searchFields) {
          if (path.slice(0, -1) === field) {    //если расположение найденного значения совпадает с
            return true                         //указанным полем для поиска при вызове, значит true
          }
        }
      }
    }
  }
  return false
}

const vehicles = [toyota, bmw, lanos];

function filterCollection(array, keyWords, every, ...searchFields) {
  const filteredArray = [];
  const keys = keyWords.split(' ');
  array.forEach((item, i) => {
    if (every === true) {
      if (keys.every((key) => {
        return searchKeys(array[i], key, searchFields)
      })) {
        filteredArray.push(array[i]);
      }
    } else {
      if (keys.some((key) => {
        return searchKeys(array[i], key, searchFields)
      })) {
        filteredArray.push(array[i]);
      }
    }
  });
  if (filteredArray.length !== 0) {
    return filteredArray
  } else {
    return 'No matches found'
  }
}

console.log('Test 1:');
console.log(filterCollection(vehicles, 'es_es sedan', true, 'description', 'type.name', 'locales.deep.name'));

console.log('Test 2:');
console.log(filterCollection(vehicles, 'manual', false, 'type.transmission.type'));