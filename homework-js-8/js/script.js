const price = document.createElement('div');
price.style.margin = '10px 5px';
const label = document.createElement('span');
label.innerText = 'Price, $ ';
const input = document.createElement('input');
price.appendChild(label);
price.appendChild(input);

const cancel = document.createElement('div');
cancel.style.cssText =
    'display: inline-block;' +
    'border: 1px solid #c3c3c3;' +
    'border-radius: 10px;' +
    'margin: 5px;' +
    'padding: 3px;';
const span = document.createElement('span');
cancel.appendChild(span);

const btn = document.createElement('span');
cancel.appendChild(btn);
btn.id = 'cancel-btn';
btn.innerText = 'Х';
btn.style.cssText =
    'display: inline-block;' +
    'width: 18px;' +
    'height: 18px;' +
    'border: 1px solid #c3c3c3;' +
    'border-radius: 50%;' +
    'margin: 0 2px;' +
    'text-align: center;' +
    'cursor: pointer;';

const alert = document.createElement('span');
alert.innerText = 'Please enter correct price';

input.addEventListener('focus', (event) => {
  event.currentTarget.style.cssText =
      'border: 2px solid green;' +
      'outline: none';
  alert.remove();
});

input.addEventListener('blur', (event) => {
  event.currentTarget.style.border = '';
  if (event.currentTarget.value < 0) {
    price.after(alert);
    event.currentTarget.style.border = '2px solid red'
  } else {
    span.innerText = `Price: ${event.currentTarget.value}`;
    container.firstChild.before(cancel.cloneNode(true));
    document.getElementById('cancel-btn').addEventListener('click', (event) => {
      event.currentTarget.parentElement.remove();
      input.value = '';
    });
    event.currentTarget.style.color = 'lightgreen';
  }
});

const container = document.createElement('div');
container.style.textAlign = 'center';
container.appendChild(price);
document.body.insertBefore(container, document.querySelector('script'));
