$('.tabs').click(function(event) {
  $('.tabs-title.active').removeClass('active');
  $(event.target).addClass('active');

  $('.tab-content.active').removeClass('active');
  $(`.tab-content[data-tab-title="${$(event.target).data('tab-title')}"]`).addClass('active')
});