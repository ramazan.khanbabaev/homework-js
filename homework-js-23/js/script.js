const newGameBtn = document.createElement('button');
newGameBtn.innerText = 'New game';
const flagsCount = document.createElement('span');
flagsCount.className = 'flags-count';
const menu = document.querySelector('.menu')
menu.appendChild(flagsCount);

const genField = (sizeI, sizeJ) => {
  const container = document.querySelector('.container');
  newGameBtn.remove();
  container.lastChild.remove(); //it's minesweeper
  const minesweeper = document.createElement('div');
  const alert = document.createElement('p');
  alert.className = 'alert';
  const cell = document.createElement('div');
  cell.classList.add('cell');
  for (let i = 0; i < sizeJ; i++) {
    for (let j = 0; j < sizeI; j++) {
      cell.id = `${i}-${j}`;
      minesweeper.append(cell.cloneNode())
    }
  }
  container.appendChild(minesweeper);
  container.style.width = `${sizeI * 50}px`;

  let clicks = 0;
  function clickOpen(event) {
    if (clicks === 0) {
      menu.prepend(newGameBtn);
      clicks++
    }
    openCell(event.target);
    if (event.target.classList.contains('digit-0')) {
      getCellsAround(event.target).forEach(cell => {
        cell.click()
      });
    }
    if (event.target.classList.contains('bomb')) {
      endGame();
    }
  }
  minesweeper.addEventListener('click', clickOpen);

  function setFlag(event) {
    event.preventDefault();
    event.target.classList.toggle('flag');
    flagsCount.innerText = `Flags set: ${ document.querySelectorAll('.flag').length} / ${amountOfBombs}`;
  }
  minesweeper.addEventListener('contextmenu', setFlag);

  function dblclickOpen(event) {
    const cellsAround = getCellsAround(event.target);
    const flagsAround = cellsAround.reduce((flagsAround, cell) =>
        cell.classList.contains('flag') ? flagsAround + 1 : flagsAround, 0);

    if (event.target.classList.contains(`digit-${flagsAround}`)) {
      cellsAround.forEach(cell => {
        if (!cell.classList.contains('flag')) {
          openCell(cell);
        }
        if (cell.classList.contains('bomb')) {
          endGame();
        }
      });
    }
  }
  minesweeper.addEventListener('dblclick', dblclickOpen);

  const endGame = () => {
    minesweeper.removeEventListener('click', clickOpen);
    minesweeper.removeEventListener('contextmenu', setFlag);
    minesweeper.removeEventListener('click', dblclickOpen);
    showBombs(bombs);
    alert.innerText = 'End game';
    minesweeper.appendChild(alert)
  }
};

let amountOfBombs = 0;
const bombs = [];

const genBombs = (amountOfBombs, sizeI, sizeJ) => {
  bombs.length = 0;
  while (bombs.length < amountOfBombs) {
    const place = `${Math.floor(Math.random()*sizeJ)}-${Math.floor(Math.random()*sizeI)}`;
    if (bombs.indexOf(place) === -1) {
      bombs.push(place);
    }
  }
  flagsCount.innerText = 'Flags set: 0 / ' + amountOfBombs;
};

const getCellsAround = (cell) => {
  const cellId = cell.id.split('-').map(id => Number(id));
  return [
    document.getElementById(`${cellId[0] - 1}-${cellId[1] - 1}`),
    document.getElementById(`${cellId[0] - 1}-${cellId[1]}`),
    document.getElementById(`${cellId[0] - 1}-${cellId[1] + 1}`),
    document.getElementById(`${cellId[0]}-${cellId[1] - 1}`),
    document.getElementById(`${cellId[0]}-${cellId[1] + 1}`),
    document.getElementById(`${cellId[0] + 1}-${cellId[1] - 1}`),
    document.getElementById(`${cellId[0] + 1}-${cellId[1]}`),
    document.getElementById(`${cellId[0] + 1}-${cellId[1] + 1}`)
  ].filter(cell => !!cell);
};

const countBombsAround = cellsAround => {
  let bombsAround = 0;
  cellsAround.forEach(cell => {
    if (bombs.includes(cell.id)) {
      bombsAround++;
    }
  });
  return bombsAround;
};

const setDigit = cell => {
  return countBombsAround(getCellsAround(cell));
};

const openCell = cell => {
  if (bombs.includes(cell.id)) {
    cell.classList.add('bomb');
    cell.style.border = '2px solid red';
  } else {
    cell.classList.add(`digit-${setDigit(cell)}`);
  }
};

const showBombs = bombs => {
  for (let id of bombs) {
    document.getElementById(id).classList.add('bomb');
  }
};

const newGame = () => {
  const sizeI = prompt('Enter amount of horizontal cells', '8');
  const sizeJ = prompt('Enter amount of vertical cells', '8');
  genField(sizeI, sizeJ);
  amountOfBombs = Math.floor(sizeI * sizeJ / 6);
  genBombs(amountOfBombs, sizeI, sizeJ);
};

newGameBtn.addEventListener('click', newGame);
newGame();