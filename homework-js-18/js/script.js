const student = {
  name:'',
  lastName:'',
  table: {}
};

student.name = prompt('Enter your name');
student.lastName = prompt('Enter your last name');

let subject, mark, sum = 0,countBadMarks = 0;
while ((subject = prompt('Enter name of subject'))) {
  mark = Number(prompt('Enter mark'));
  student.table[subject] = mark;
  sum += mark;
  if (mark < 4) {
    countBadMarks++;
  }
}

console.log(student);

const avgMarks = (sum / Object.keys(student.table).length).toFixed(2);

console.log('Bad marks: ', countBadMarks);
if (countBadMarks === 0) {
  alert('Student will transferred to the next course')
}

console.log('Average of marks: ', avgMarks);
if (avgMarks > 7) {
  alert('Student will have a scholarship.');
}



