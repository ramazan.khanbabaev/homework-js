const createElement = element => {
  if (Array.isArray(element)) {
    return `<ul>${element.reduce((list, listElement) => list.concat(createElement(listElement)), '')}</ul>`
  } else if (typeof element === 'object') {
    return createElement(Object.keys(element).map(key => `${key}: ${element[key]}`))
  } else {
    return `<li>${element}</li>`
  }
};

const showArray = array => {
  const list = document.createElement('ul');
  array.forEach(item => {
    list.innerHTML += createElement(item);
  });
  document.body.insertBefore(list, document.body.lastChild);
};

const deleteAfterTime = (element ,timeout) => {
  const timerCount = document.createElement('p');
  timerCount.innerText = `Remaining time: ${timeout}`;
  document.body.insertBefore(timerCount, document.body.firstChild);
  setInterval(() => {
    timerCount.innerText = `Remaining time: ${--timeout}`;
  }, 1000);

  setTimeout(() => {
    document.body.removeChild(element);
    document.body.removeChild(timerCount)
  }, timeout * 1000);
};

showArray(['hello', 'world', 'Kiev', [1, 2, 3, {name: 'Alice', sex: 'female'}, 4, 5], 273, {name: 'Bob', age: 18}]);

deleteAfterTime(document.querySelector('ul'),10);
